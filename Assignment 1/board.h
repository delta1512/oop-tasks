#include "constants.h"

#ifndef Board_H
#define Board_H


using namespace std;


class Board {
  char** board;
  int size;

  void getPrevXY(int x, int y, char playerIcon, int &prevX, int &prevY);

  public:
    Board(int boardSize);
    char** getBoard();
    bool isInBoard(int x, int y);
    void show();
    void clear();
    void updateBoard(int x, int y, char playerIcon);
    status processMove(int x, int y, char playerIcon);
    ~Board();
};


#endif
