/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <iostream>
#include <time.h>
#include <ctime>
#include <cstdlib>

#include "humanPlayer.h"
#include "randomPlayer.h"
#include "smartPlayer.h"
#include "constants.h"
#include "board.h"


status getGameStatus(status p1Stat, status p2Stat);


int main() {
  // Set up sleep configuration
  // This is so that rand vs rand games don't end instantly
  struct timespec t1, t2; // Generate start and end time structs
  t1.tv_sec = 0; // Clear the seconds value (we want less than a second)
  t1.tv_nsec = 200000000L; // 0.2s

  // Set up the board with a user-defined size
  int boardSize;
  do {

    cout << "\nGame board size: ";
    cin >> boardSize;

  } while ( (boardSize < 4) || (boardSize > 20) );

  // Initialise the board and players
  Board tron(boardSize);
  Player* players[2];
  status results[2];      // Tracks the results of either player's moves
  status gameStatus;      // Tracks whether game is over

  // Set up player 1 and 2
  char choice;
  int startPos;
  char iconChoice[2] = {P1_ICON, P2_ICON};
  for (int i = 0; i < 2; i++) {

    do {

      cout << "\nPlayer " << i+1 << " human (h), random (r) or smart (s): ";
      cin >> choice;

    } while ( !((choice == 'h') || (choice == 'r') || (choice == 's')) );

    // start at 0, 0 for p1 and boardSize-1, boardSize-1 for p2
    startPos = (boardSize-1) * i;

    // Set up the respective player
    switch (choice) {
      case 'h':
        players[i] = new HumanPlayer(startPos, startPos, iconChoice[i]);
        break;
      case 'r':
        players[i] = new RandomPlayer(startPos, startPos, iconChoice[i]);
        break;
      case 's':
        players[i] = new SmartPlayer(startPos, startPos, iconChoice[i]);
        break;
      default:
        break;
    }

  }


  // MAIN GAME LOOP
  Player* cPlayer;
  char nextMatch;
  do {

    // Reset the game status
    gameStatus = CONTINUE;
    // Reset player locations
    players[0]->reset();
    players[1]->reset();
    // Clear the game board
    tron.clear();
    // Initialise the initial places of the players
    tron.processMove(players[0]->getX(), players[0]->getY(), players[0]->getIcon());
    tron.processMove(players[1]->getX(), players[1]->getY(), players[1]->getIcon());

    while (gameStatus == CONTINUE) {

      // Display the board
      tron.show();

      // For each user
      for (int i = 0; i < 2; i++) {

        // Get the pointer of the current player
        cPlayer = players[i];
        // Get input for making a move
        cPlayer->changeDirection(cPlayer->getNewDirection(tron));
        // Move the player
        cPlayer->move();
        // Send the move to the board and save the  outcome
        results[i] = tron.processMove(cPlayer->getX(), cPlayer->getY(), cPlayer->getIcon());

      }

      // Check whether game is won, drawn or still going
      gameStatus = getGameStatus(results[0], results[1]);
      // Sleep for 0.2s
      nanosleep(&t1, &t2);

    }

    // Show the final board after game has ended
    tron.show();

    // Display who won and award points
    cout << "\n\n\n";
    switch (gameStatus) {
      case P1:
        cout << "Player 1 wins";
        players[0]->win();
        break;
      case P2:
        cout << "Player 2 wins";
        players[1]->win();
        break;
      case DRAW:
        cout << "Draw";
        break;
    }
    cout << "!!!\n";

    // Display the final scores
    cout << "\nScores:\nPlayer 1: " << players[0]->getScore()
         << "\nPlayer 2: " << players[1]->getScore() << endl;

    // Prompt for another game
    cout << "\nPlay another game? (y/n): ";
    cin >> nextMatch;

  } while (nextMatch == 'y' || nextMatch == 'Y');

  return 0;
}


// Checks both players results from moving to see who won or if there
// was a draw.
status getGameStatus(status p1Stat, status p2Stat) {
  // Case where both players die at the same time
  if (p1Stat == P2 && p2Stat == P1) {
    return DRAW;
  }

  // If p1Stat doesn't say CONTINUE, then return it
  if (p1Stat != CONTINUE) {
    return p1Stat;

  // else, p2Stat is the only other value we have so return it
  } else {
    return p2Stat;
  }
}
