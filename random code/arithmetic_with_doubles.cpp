#include <iostream>
#include <string>

using namespace std;

int main() {
  int doublediv = 1.0 / 2.0; // Int Truncates decimal
  cout << doublediv << endl;

  double intdiv = 10 / 3; // division truncates decimal, double stores int component
  cout << intdiv << endl;

  cout << 1.0 / 2 << endl; // division with one double auto-casts to double, hence all parts conserved
  cout << 1 / 2.0 << endl;

  cout << 1 / 2 << endl; // Division with ints truncates decimal

  return 0;
}
