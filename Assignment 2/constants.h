#include <math.h>
#include <string>

#ifndef CONSTANTS_H
#define CONSTANTS_H


// Configuration constants
const double MIN_PRICE = 50;
const double MAX_PRICE = 150.00;
const int MIN_UNITS = 1;
const int MAX_UNITS = 50;

// Types of bids
const char BUY = 'B';
const char SELL = 'S';
const char TYPES[2] = {BUY, SELL};

// Spacing constants
// The maximum width of the price when printed (+1 for log rule +4 for precision)
const int MAX_PRICE_W = (int)log10(MAX_PRICE) + 5;
// The maximum width of the units when printed (+1 for log rule)
const int MAX_UNITS_W = (int)log10(MAX_UNITS) + 1;
// The maximum width when the volume totals are printed (+6 for buffer)
const int MAX_VOL_W = (int)log10(MAX_PRICE_W*MAX_UNITS_W) + 6;

// The case where a trader is both a buyer and seller
const char ALL_TRADER = 'N';

// The basedir for the stored auction results
const std::string BASEDIR = "";

#endif
