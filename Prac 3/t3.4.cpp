#include <iostream>
#include <iomanip>

using namespace std;

const int ROWS = 8;
const int COLS = 4;
const int MAX_BOOKINGS = ROWS*COLS;

void getBooking(char seats[ROWS][COLS], int &row, int &col);
void displayBookings(char seats[ROWS][COLS]);
bool isValidBooking(char seats[ROWS][COLS], int row, int col);


int main() {
  char seats[ROWS][COLS];
  int row, col, numBookings = 0;

  // Fill the array with initial values
  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      seats[i][j] = '_';
    }
  }

  while (numBookings < MAX_BOOKINGS) {
    displayBookings(seats);
    getBooking(seats, row, col);
    if (row == -1) {
      break;
    }
    seats[row][col] = 'X';
    numBookings++;
  }

  displayBookings(seats);
  cout << "Bookings completed!" << endl;

  return 0;
}


void getBooking(char seats[ROWS][COLS], int &row, int &col) {
  do {
    cout << "Type row number (-1 to finish): ";
    cin >> row;
    if (row == -1) {
      break;
    }
    cout << "Type a column number: ";
    cin >> col;
    row--;
    col--;
  } while (!isValidBooking(seats, row, col));
}


void displayBookings(char seats[ROWS][COLS]) {
  cout << "  1 2 3 4" << endl;
  for (int i = 0; i < ROWS; i++) {
    cout << i+1 << " ";
    for (int j = 0; j < COLS; j++) {
      cout << seats[i][j] << " ";
    }
    cout << endl;
  }
}



bool isValidBooking(char seats[ROWS][COLS], int row, int col) {
	// If the indices are valid, check whether the index is taken
  // -1 is valid row because it is an escape
	if ((row >= 0) && (row < ROWS) && (col >= 0) && (col < COLS)) {
		if (seats[row][col] == '_') {
			// If the seat is free, it is valid
			return true;
		}
    cout << endl << "Seat is taken, choose another." << endl << endl;
	}

	// Else return false
	return false;
}
