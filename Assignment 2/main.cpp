/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

#include <iostream>
#include <iomanip>
#include <assert.h>
#include <cstdlib>

#include "random.h"
#include "constants.h"
#include "bid.h"
#include "trader.h"
#include "buyer.h"
#include "seller.h"
#include "simulator.h"

using namespace std;


void bidDriver();
void traderDriver();
void simulatorDriver();
int getRand(int a, int b);
double getRand(double n);


int main() {
  // Seed RNG
  srand(time(NULL));
  // Set the floating point precision to be 2 decimal places
  cout.setf(ios::fixed);
  cout.setf(ios::showpoint);
  cout.precision(2);

  // Call the drivers for testing
  bidDriver();
  traderDriver();
  simulatorDriver();

  return 0;
}


void bidDriver() {
  int assertions = 10; // Test 10 bids
  int fillQ, units, unitsLeft;
  double fillP, clearPrice;
  Bid* testbid;

  cout << "--- BID.H DRIVER ---\n";
  cout << "Testing " << assertions << " assertions...\n";

  for (int i = 0; i < assertions; i++) {
    // Create some new randomised bid
    units = getRand(0, MAX_UNITS);
    testbid = new Bid("Marcus", TYPES[getRand(0, 1)],
                      getRand(MAX_PRICE), units);
    // Print it
    testbid->print();
    // Get some random values to fill the order with
    fillQ = getRand(1, MAX_UNITS);
    fillP = getRand(testbid->getPrice());
    // Fill the order
    testbid->fill(fillQ, fillP, clearPrice, unitsLeft);
    // Test the outputs
    assert(clearPrice == (testbid->getPrice()+fillP)/2);
    // Do different assertions depending if the bid is filled or not
    if (fillQ >= units) {
      assert(testbid->isFilled());
      assert(unitsLeft == abs(units-fillQ));
      assert(testbid->getQuantity() == 0);
    } else {
      assert(!testbid->isFilled());
      assert(unitsLeft == 0);
      assert(testbid->getQuantity() == units-fillQ);
    }

    // Garbage collect the object that was just created
    delete testbid;
  }

  cout << "--- END BID.H DRIVER ---\n";
}


// TODO: 10 buy bids and 10 sell bids
void traderDriver() {
  int n = 10;
  Buyer* buyers[n];
  Seller* sellers[n];

  cout << "--- TRADER.H DRIVER ---\n";

  // Initialise trader arrays
  for (int i = 0; i < n; i++) {
    buyers[i] = new Buyer(getRandName());
    sellers[i] = new Seller(getRandName());
  }

  // Create 10 buys and 10 sells
  for (int i = 0; i < n; i++) {
    cout << "Current buyer: " << buyers[i]->getName() << endl;
    cout << "Their bid: ";
    buyers[i]->generateBid()->print();
    cout << "Current seller: " << sellers[i]->getName() << endl;
    cout << "Their bid: ";
    sellers[i]->generateBid()->print();
  }

  // Test the different types of manual bid creation techniques
  string name;
  cout << "Your name: ";
  getline(cin, name);

  // Make a normal trader, buyer and seller
  Trader normal_trader(name, ALL_TRADER);
  Buyer buyer(name);
  Seller seller(name);

  // Test manual input
  cout << "Make your order as a normal trader:\n";
  normal_trader.generateManualBid()->print();
  cout << "Make your order as a buyer:\n";
  buyer.generateManualBid()->print();
  cout << "Make your order as a seller:\n";
  seller.generateManualBid()->print();

  // Clean up memory
  for (int i = 0; i < n; i++) {
    // Delete each buyer
    delete buyers[i];
    // Delete each seller
    delete sellers[i];
  }

  cout << "--- END TRADER.H DRIVER ---\n";
}


void simulatorDriver() {
  int nTraders = 10;
  int nOrders = 10;
  Simulator sim(nTraders, nTraders);

  cout << "--- SIMULATOR.H DRIVER ---\n";

  // Test the simulation with random, automatic orders
  sim.getBuyOrders(nOrders);
  sim.getSellOrders(nOrders);
  sim.doAuction();

  // Test the simulation with manual bids
  sim.getBuyOrders(nOrders, true);
  sim.getSellOrders(nOrders, true);
  sim.doAuction();

  // Run the interactive simulation with more orders
  cout << "Now running interactive simulation...\n";
  sim.run(50);

  cout << "--- END SIMULATOR.H DRIVER ---\n";
}
