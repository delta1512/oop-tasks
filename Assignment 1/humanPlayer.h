#include "player.h"
#include "board.h"

#ifndef HumanPlayer_H
#define HumanPlayer_H


class HumanPlayer: public Player {
  public:
    HumanPlayer(int startX, int startY, char ico);
    char getNewDirection(Board &board);
};


#endif
