#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>

using namespace std;

const int ROWS=3;
const int COLS=4;

void makeTable(int t[ROWS][COLS]);
void printTable(int t[ROWS][COLS]);
void getNeighbours(int &up, int &down, int &left, int &right, int col, int row, int t[ROWS][COLS]);
bool isValidIndex(int col, int row);


int main() {
	srand(time(0));

	int table[ROWS][COLS], col, row;
	int up = 99, down = 99, left = 99, right = 99; // 99 is sentinel value

	makeTable(table);
	printTable(table);

	cout << "column-value: ";
	cin >> col;
	cout << "row-value: ";
	cin >> row;
	// Convert to indexation from 0
	col--;
	row--;

	getNeighbours(up, down, left, right, col, row, table);

	// If any value is 99, don't print it
	if (up != 99) {
		cout << "up:\t" << up << endl;
	}
	if (down != 99) {
		cout << "down:\t" << down << endl;
	}
	if (left != 99) {
		cout << "left:\t" << left << endl;
	}
	if (right != 99) {
		cout << "right:\t" << right << endl;
	}

	return 0;
}


void makeTable(int t[ROWS][COLS]) {
	for (int i = 0; i < ROWS; i ++) {
		for (int j = 0; j < COLS; j++) {
			t[i][j] = rand() % 3 - 1;
		}
	}
}


void printTable(int t[ROWS][COLS]) {
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			cout << setw(3) << t[i][j];
		}
		cout << endl;
	}
}


void getNeighbours(int &up, int &down, int &left, int &right, int col, int row, int t[ROWS][COLS]) {
	if (isValidIndex(row-1, col)) {
		up = t[row-1][col];
	}

	if (isValidIndex(row+1, col)) {
		down = t[row+1][col];
	}

	if (isValidIndex(row, col-1)) {
		left = t[row][col-1];
	}

	if (isValidIndex(row, col+1)) {
		right = t[row][col+1];
	}
}


bool isValidIndex(int row, int col) {
	return (col >= 0) && (col < COLS) && (row >= 0) && (row < ROWS);
}
