//Searches a partially filled array of nonnegative integers.
#include <iostream>

using namespace std;

const int DECLARED_SIZE = 10;

void fillArray(int a[], int size, int &numberUsed) {
    cout << "Enter up to " << size << " nonnegative whole numbers.\n"
         << "Mark the end of the list with a negative number.\n";

    int next, index = 0;
    cin >> next;
    while ((next >= 0) && (index < size)) {
        a[index] = next;
        index++;
        cin >> next;
    }

    numberUsed = index;
}


// Added &index to keep track of where we have searched in the array
int search(const int a[], int &index, int listSize, int target) {
    bool found = false;
    while ((!found) && (index < listSize)) {
      if (target == a[index]) {
        found = true;
      }
      // Increment the index
      // This will get saved for the next function call (pass-by-ref)
      index++;
    }

    if (found) {
        return index-1;
    } else {
        return -1;
    }
}


int main() {
    int arr[DECLARED_SIZE], listSize, target;

    fillArray(arr, DECLARED_SIZE, listSize);

    char ans;
    // Index tracks where we are in the array
    int result, index;

    do {
      index = 0; // Reset to zero when new search started
      cout << "Enter a number to search for: ";
      cin >> target;

      result = search(arr, index, listSize, target);

      // Handle the case where the term is not in the list
      if (result == -1) {

        cout << target << " is not in the list.\n";

      } else {

        cout << target << " is stored in array position(s) ";

        // While there is a term in the array
        while (result != -1) {
          // Print the result - 1
          // (because we increment to the next index)
          cout << result << " ";
          // Search for the next term
          result = search(arr, index, listSize, target);
        }

        cout << endl << "(Remember: The first position is 0.)\n";

      }

      cout << "Search again?(y/n followed by return): ";
      cin >> ans;

    } while ((ans != 'n') && (ans != 'N'));

    cout << "End of program.\n";
    return 0;
}
