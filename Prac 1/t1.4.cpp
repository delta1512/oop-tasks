#include <iostream>

using namespace std;

int main() {
  int start = 1;
  int end = 10;

  // Print the top row
  cout << "  | "; // Spacing for the left corner of the table
  for (int i = start; i < end + 1; i++) {
    cout << i << " ";
  }

  cout << endl;

  // 2* for spacing, +4 for the corner
  for (int i = 0; i < 2*end + 4; i++) {
    cout << "-";
  }

  cout << endl;

  for (int i = start; i < end + 1; i++) {
    cout << i << " | ";
    for (int j = start; j < end + 1; j++) {
      cout << i*j << " ";
    }
    cout << endl;
  }

  return 0;
}
