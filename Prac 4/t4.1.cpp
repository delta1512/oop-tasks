//Searches a partially filled array of nonnegative integers.
#include <iostream>

using namespace std;

const int DECLARED_SIZE = 10;


class oneDArray {
  private:
    int array[DECLARED_SIZE];
    int numberUsed;

  public:
    void fillArray();
    int search(int target);
};


void oneDArray::fillArray() {
  cout << "Enter up to " << DECLARED_SIZE << " nonnegative whole numbers.\n"
       << "Mark the end of the list with a negative number.\n";

  int next, index = 0;
  cin >> next;
  while ((next >= 0) && (index < DECLARED_SIZE))
  {
      array[index] = next;
      index++;
      cin >> next;
  }

  numberUsed = index;
}


int oneDArray::search(int target) {
  int index = 0;
  bool found = false;
  while ((!found) && (index < numberUsed)) {
    if (target == array[index]) {
      found = true;
    } else {
      index++;
    }
  }

  if (found) {
    return index;
  } else {
    return -1;
  }
}


int main() {
    oneDArray a;
    int target, result;
    char ans;

    a.fillArray();

    do {
      cout << "Enter a number to search for: ";
      cin >> target;

      result = a.search(target);

      if (result == -1)
          cout << target << " is not on the list.\n";
      else
          cout << target << " is stored in array position "
               << result << endl
               << "(Remember: The first position is 0.)\n";

      cout << "Search again?(y/n followed by return): ";
      cin >> ans;
    } while ((ans != 'n') && (ans != 'N'));


    return 0;
}
