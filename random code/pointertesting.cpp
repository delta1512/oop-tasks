#include <iostream>

using namespace std;

int main() {
  int *ptr;
  int value = 10;

  ptr = &value;

  cout << *ptr << endl; // 10
  *ptr = 200;
  cout << *ptr << endl; // 200
  cout << value << endl; // 200
  value = 69;
  cout << *ptr << endl; // 69
  cout << ptr << endl; // address
}
