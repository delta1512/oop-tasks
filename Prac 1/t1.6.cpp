#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int getRand(int, int);


int main() {
  srand(time(NULL)); // Changes the seed so that the same random numbers are not provided

  int num = getRand(1, 15);
  int guess = 0;
  int numGuess = 0;

  while (numGuess < 3) {
    cout << "Guess my number: ";
    cin >> guess;

    if (guess == num) {
      cout << "You win!" << endl;
      return 0;
    } else {
      cout << "Incorrect, ";

      if (guess > num) {
        cout << "Too high";
      } else {
        cout << "Too low";
      }
      cout << endl;

      numGuess++;
    }
  }

  cout << "You lose! The number is " << num << endl;

}

// Returns a random number between a and b (inclusive)
int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}
