/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

#include "simulator.h"


using namespace std;


// CONSTRUCTOR
Simulator::Simulator(int numBuyers, int numSellers) {
  // Initialise the buyers
  nBuyers = numBuyers;
  buyers = new Buyer*[nBuyers];
  for (int i = 0; i < nBuyers; i++) {
    buyers[i] = new Buyer(getRandName());
  }

  // Initialise the sellers
  nSellers = numSellers;
  sellers = new Seller*[nSellers];
  for (int i = 0; i < nSellers; i++) {
    sellers[i] = new Seller(getRandName());
  }
}


// Generates a random set of n buy orders
//  manual (optional): whether or not to make bids with user input
void Simulator::getBuyOrders(int n, bool manual) {
  // Flag that orders needs cleaning when ready
  ordersCleared = false;
  nBuys = n;
  buys = new Bid*[n];
  for (int i = 0; i < n; i++) {
    if (manual) {
      buys[i] = buyers[getRand(nSellers-1)]->generateManualBid();
    } else {
      buys[i] = buyers[getRand(nBuyers-1)]->generateBid();
    }
  }
}


// Same as above but with sell orders
void Simulator::getSellOrders(int n, bool manual) {
  // Flag that orders needs cleaning when ready
  ordersCleared = false;
  nSells = n;
  sells = new Bid*[n];
  for (int i = 0; i < n; i++) {
    if (manual) {
      sells[i] = sellers[getRand(nSellers-1)]->generateManualBid();
    } else {
      sells[i] = sellers[getRand(nSellers-1)]->generateBid();
    }
  }
}


// Safely cleans up memory when the next set of orders comes through
void Simulator::clearOrders() {
  // To prevent double free() error, check if orders were deleted already
  if (ordersCleared) {
    return;
  }

  for (int i = 0; i < nBuys; i++) {
    delete buys[i];
  }
  delete[] buys;

  for (int i = 0; i < nSells; i++) {
    delete sells[i];
  }
  delete[] sells;

  // Flag that the orders were cleared
  ordersCleared = true;
}


// Handles the auctioneer and processing of orders
// Precondition: getBuyOrders() and getSellOrders() must be called
// Postcondition: Orders will be deleted (the precondition is reverted)
void Simulator::doAuction() {
  // If the Auctioneer hasn't been initialised, initialise it
  if (auctioneer == NULL) {
    auctioneer = new Auctioneer(buys, nBuys, sells, nSells);
  } else {
    auctioneer->reset(buys, nBuys, sells, nSells);
  }

  // Run the matching on all the orders
  auctioneer->run();

  // Garbage collection
  clearOrders();
}


// Easy interactive simulator
void Simulator::run(int maxOrders) {
  char choice;
  do {
    // Make some random orders
    getBuyOrders(getRand(1, maxOrders));
    getSellOrders(getRand(1, maxOrders));

    // Match them in the double auction
    doAuction();

    // Prompt the user for more input
    cout << "Auction number " << auctioneer->getNumAucts() << " complete. Continue? [y/n]: ";
    cin >> choice;
  } while (choice == 'y' || choice == 'Y');
}


// DESTRUCTOR
Simulator::~Simulator() {
  // Delete all the trades that were made
  clearOrders();

  // Delete all the traders that exist in the simulation
  for (int i = 0; i < nBuyers; i++) {
    delete buyers[i];
  }
  delete[] buyers;

  for (int i = 0; i < nSellers; i++) {
    delete sellers[i];
  }
  delete[] sellers;
}
