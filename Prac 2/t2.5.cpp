#include <iostream>

using namespace std;


void sort(int &a, int &b, int &c);
void swap(int &n1, int &n2);


int main() {
  int a, b, c;

  cout << "Int a: ";
  cin >> a;
  cout << "Int b: ";
  cin >> b;
  cout << "Int c: ";
  cin >> c;

  sort(a, b, c);

  cout << endl << a << " " << b << " " << c << endl;

  return 0;
}


void sort(int &a, int &b, int &c) {
  for (int i = 0; i < 3; i++) {
    if (a > b) {
      swap(a, b);
    }
    if (b > c) {
      swap(b, c);
    }
  }
}


void swap(int &n1, int &n2) {
  int tmp = n1;
  n1 = n2;
  n2 = tmp;
}
