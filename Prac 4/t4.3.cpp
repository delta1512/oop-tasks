#include <iostream>
#include <iomanip>

using namespace std;

const int ROWS = 8;
const int COLS = 4;
const int MAX_BOOKINGS = ROWS*COLS;


class Bookings {
  private:
    char seats[ROWS][COLS];
    int numBookings = 0;

    bool isValidBooking(int row, int col);

  public:
    Bookings();
    void showBookings();
    void makeBooking();
    bool isFinished();
};


Bookings::Bookings() {
  // Start by filling the array with initial values
  for (int i = 0; i < ROWS; i++) {
    for (int j = 0; j < COLS; j++) {
      seats[i][j] = '_';
    }
  }
}


bool Bookings::isFinished() {
  return numBookings == MAX_BOOKINGS;
}


void Bookings::showBookings() {
  cout << "  1 2 3 4\n";
  for (int i = 0; i < ROWS; i++) {
    cout << i+1 << " ";
    for (int j = 0; j < COLS; j++) {
      cout << seats[i][j] << " ";
    }
    cout << endl;
  }
}


void Bookings::makeBooking() {
  int row, col;
  cout << "Type row number (-1 to finish): ";
  cin >> row;
  // If the user wants to exit, finish the bookings
  if (row == -1) {
    numBookings = MAX_BOOKINGS;
    return;
  }
  cout << "Type a column number: ";
  cin >> col;

  // Fix the index and check validity, then set or prompt invalid
  if (isValidBooking(--row, --col)) {
    seats[row][col] = 'X';
    numBookings++;
  } else {
    cout << "\nInvalid booking, try again.\n\n";
  }
}


bool Bookings::isValidBooking(int row, int col) {
	// If the indices are valid, check whether the index is taken
  // -1 is valid row because it is an escape
	if ((row >= 0) && (row < ROWS) && (col >= 0) && (col < COLS)) {
		if (seats[row][col] == '_') {
			// If the seat is free, it is valid
			return true;
		}
    cout << "\nSeat is taken.";
	}

	// Else return false
	return false;
}


int main() {
  Bookings bookings;

  while (!bookings.isFinished()) {
    bookings.showBookings();
    bookings.makeBooking();
  }

  cout << "Bookings have been completed.\n";
}
