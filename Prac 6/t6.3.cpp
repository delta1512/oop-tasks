//Program to demonstrate the CDAccount structure type.
#include <iostream>
using namespace std;

class Date {
    int day;
    int month;
    int year;

  public:
    void getDate();
    int getDay() {return day;}
    int getMonth() {return month;}
    int getYear() {return year;}
};

void Date::getDate() {
    cout << "\nEnter month: ";
    cin >> month;
    cout << "Enter day: ";
    cin >> day;
    cout << "Enter year: ";
    cin >> year;
}


class CDAccount {
  double initialBalance;
  double interestRate;
  int term;//months until maturity
  Date maturity; //date when CD matures

  public:
    void getCDData();
    double balanceAtMaturity();
    void printAccount();
};

void CDAccount::getCDData() {
    cout << "Enter account initial balance: $";
    cin >> initialBalance;
    cout << "Enter account interest rate: ";
    cin >> interestRate;
    cout << "Enter the number of months until maturity: ";
    cin >> term;
    cout << "Enter the maturity date:";
    maturity.getDate();
}

double CDAccount::balanceAtMaturity() {
  double rateFraction, interest;
  rateFraction = interestRate/100.0;
  interest = initialBalance*(rateFraction*(term/12.0));
  return initialBalance + interest;
}

void CDAccount::printAccount() {
  cout.setf(ios::fixed);
  cout.setf(ios::showpoint);
  cout.precision(2);

  cout << "When the CD matured on "
       << maturity.getDay()
       << "-"<< maturity.getMonth() << "-"
        << maturity.getYear() << endl
       << "it had a balance of $"
       << balanceAtMaturity() << endl;
}



int main() {
    CDAccount account;
    cout << "Enter account data on the day account was opened:\n";
    account.getCDData();

    account.printAccount();

    return 0;
}
