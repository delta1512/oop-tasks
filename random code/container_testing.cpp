#include <vector>
#include <list>
#include <queue>
#include <set>
#include <map>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <iostream>


using namespace std;


const int chars[] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 'a'};


// Returns a random integer between a and b (inclusive)
int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}


int main() {
  srand(time(NULL));
  vector<int> v;
  list<int> l;
  queue<int> q;
  set<int> s;
  map<char, int> m;

  // Fill the structures
  int rnum;
  for (int i = 0; i < 10; i++) {
    rnum = getRand(1, 10);
    v.push_back(rnum);
    l.push_back(rnum);
    q.push(rnum);
    s.insert(rnum);
    m[chars[i]] = rnum;
    cout << rnum << " ";
  }

  cout << endl;

  // Print the things within the structures
  vector<int>::iterator itorv;
  cout << "Vector: ";
  for (itorv = v.begin(); itorv != v.end(); itorv++) {
    cout << *itorv << " ";
  }
  cout << endl;

  list<int>::iterator itorl;
  cout << "List: ";
  for (itorl = l.begin(); itorl != l.end(); itorl++) {
    cout << *itorl << " ";
  }
  cout << endl;

  cout << "Queue: ";
  for (int i = 0; i < 10; i++) {
    cout << q.front() << " ";
    q.pop();
  }
  cout << endl;

  set<int>::iterator itors;
  cout << "Set: ";
  for (itors = s.begin(); itors != s.end(); itors++) {
    cout << *itors << " ";
  }
  cout << endl;

  cout << "Map: ";
  for (int i = 0; i < 10; i++) {
    cout << m[chars[i]] << " ";
  }
  cout << endl << endl;


  cout << "min/max functions: " << endl;
  cout << *max_element(v.begin(), v.end()) << endl;
  cout << *min_element(v.begin(), v.end()) << endl;
  cout << *max_element(l.begin(), l.end()) << endl;
  cout << *min_element(l.begin(), l.end()) << endl;

  return 0;
}
