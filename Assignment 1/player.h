#include "board.h"
#include "constants.h"

#ifndef Player_H
#define Player_H


class Player {
  protected:
    // Starting x,y
    int xInit;
    int yInit;
    // Instantaneous x,y
    int xPos;
    int yPos;
    // Starting direction
    const int* startDir;
    // Instantaneous direction
    const int* direction;
    // Character that the player appears as on the board
    char icon;
    int score = 0;

    bool isValidDirection(const int* dir);
    const int* getDirectionFromChar(char dir);
    void getPossibleMoves(int &numMoves, char possibleMoves[], Board &board);

  public:
    Player(int startX, int startY, char ico);
    int getX();
    int getY();
    char getIcon();
    int getScore();
    void move();
    void win();
    void reset();
    void changeDirection(char newDirection);

    // Every subclass will use getNewDirection() except for Player itself
    virtual char getNewDirection(Board &board) = 0;
};


#endif
