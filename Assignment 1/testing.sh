#!/bin/bash

n=0;
success=0;

while [[ $n -lt 100 ]]; do
  n=$(( n + 1 ));
  if echo -e "5\ns\nr" | ./a.out | grep -q "Player 1 wins"; then
    success=$(( success + 1 ));
    #echo "success is $success";
  fi
done

echo -e "Total: $n \nWins: $success";
