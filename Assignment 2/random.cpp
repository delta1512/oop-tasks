/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

// Small library of functions that implement randomised functionality
#include "random.h"

// The number of names in the below array
const int NUM_NAMES = 20;

// A list of names to be picked randomly
const std::string NAMES[] = {
  "Matt", "Jason", "Clyde", "Harry", "Jack",
  "Mark", "Bob", "Cody", "Jesse", "Chad",
  "Mary", "Jane", "Samantha", "Emily", "Jemma",
  "Kathrine", "Gretta", "Sandra", "Alice", "Anne"
};


// Returns a random integer between a and b (inclusive)
int getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}

// Overloaded getRand to get integers between 0 and n (inclusive)
int getRand(int n) {
  return getRand(0, n);
}


// Overloaded getRand to get doubles between a and b
double getRand(double a, double b) {
  return a + (rand() / (double)RAND_MAX) * (b - a);
}


// Overloaded getRand to get doubles between 0 and n
double getRand(double n) {
  return getRand(0.0, n);
}


// Gets a random name from the list above
std::string getRandName() {
  return NAMES[getRand(NUM_NAMES-1)];
}
