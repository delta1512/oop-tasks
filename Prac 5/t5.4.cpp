#include <iostream>

using namespace std;


class Student {
  string name;
  int numUnits;
  string* unitList;

  public:
    Student(string studentName, int units) {
      name = studentName;
      numUnits = units;
      unitList = new string[numUnits];
    }

    void setUnitNames() {
      for (int i = 1; i <= numUnits; i++) {
        cout << "Enter the name for unit " << i << ": ";
        getline(cin, unitList[i-1]);
      }
    }

    void display() {
      cout << endl << name << endl << endl;
      cout << "Currently enrolled in: " << endl;
      for (int i = 0; i < numUnits; i++) {
        cout << unitList[i] << endl;
      }
    }

    // Deallocate the memory for the array
    ~Student() {
      delete[] unitList;
    }
};


int main() {
  // Create a new student Sally with her name and units
  Student sally("Sally Fitzroy", 3);

  // Get the unit names from input
  sally.setUnitNames();
  // Display all the information about Sally
  sally.display();

  return 0;
}
