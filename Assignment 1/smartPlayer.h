#include "player.h"

#ifndef SmartPlayer_H
#define SmartPlayer_H


class SmartPlayer: public Player {
    bool isDeadEnd(int x, int y, Board &board);

  public:
    SmartPlayer(int startX, int startY, char ico);
    char getNewDirection(Board &board);
};


#endif
