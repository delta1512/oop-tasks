#include <iostream>

using namespace std;

void test1() {
  int test1Scope = 69;
  void test2() {
    cout << test1Scope << endl;
  }
}

int main() {
  int mainScope = 69;

  test1();

  return 0;
}
