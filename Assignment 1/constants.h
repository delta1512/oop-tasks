#ifndef Constants_H
#define Constants_H


// x, y pairs that define directions
// these values will be added to the xPos and yPos respectively
const int UP[]    = {-1,   0};
const int DOWN[]  = {1 ,   0};
const int LEFT[]  = {0 ,  -1};
const int RIGHT[] = {0 ,   1};

// All the possible moves as characters
const char MOVES[] = {'u', 'r', 'd', 'l'};

// All the board characters
const char P1_ICON = 'R';
const char P2_ICON = 'B';
const char TRAIL = 'O';
const char EXPLODE = 'X';
const char SPACE = ' ';

// Game status enums
enum status {P1, P2, DRAW, CONTINUE};


#endif
