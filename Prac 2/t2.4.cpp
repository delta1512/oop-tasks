#include <iostream>

using namespace std;


void getQandR(int& n, int& d, int& q, int& r);


int main() {
  int n, d, q, r;

  cout << "Type a numerator: ";
  cin >> n;
  cout << "Type a denominator: ";
  cin >> d;

  getQandR(n, d, q, r);

  cout << "\nQuotient: " << q << endl;
  cout << "Remainder: " << r << endl;

  return 0;
}


void getQandR(int &n, int &d, int &q, int &r) {
  q = n / d;
  r = n % d;
}
