#include <sstream>
#include <iomanip>
#include <string>

#include "constants.h"

#ifndef TRADE_H
#define TRADE_H


struct Trade {
  std::string maker; // The trader that caused the trade to be filled
  std::string taker; // The trader that had an order waiting to be filled
  // The IDs of the trade
  int makerOID;
  int takerOID;
  int volume; // The amount of units that were filled
  double clearPrice;

  std::string toString();
};


#endif
