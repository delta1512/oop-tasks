#include <iostream>
#include <cstring>

using namespace std;


class DynamicArray {
  private:
    int* darray;

  public:
    int length = 0;

    void push(int i) {
      if (length > 0) {
        int* tmparr = new int[++length];
        memcpy(tmparr, darray, sizeof(length)*(length-1));
        delete[] darray;
        darray = tmparr;
        darray[length-1] = i;
      } else {
        darray = new int[++length];
        darray[0] = i;
      }
    }

    void show() {
      for (int i = 0; i < length; i++) {
        cout << darray[i] << " ";
      }
      cout << endl;
    }

    ~DynamicArray() {delete[] darray;}
};

int main() {
  DynamicArray test;

  test.push(10);
  test.show();
  test.push(11);
  test.show();
}
