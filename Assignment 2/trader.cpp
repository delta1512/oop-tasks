/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

#include "trader.h"


using namespace std;


// Generates a random bid based on the type of trader
Bid* Trader::generateBid() {
  // If a general trader, then pick a random buy or sell
  if (traderType == ALL_TRADER) {
    return new Bid(traderName, TYPES[getRand(0, 1)], getRand(MAX_PRICE), getRand(1, MAX_UNITS));
  }
  // Else include the traderType
  return new Bid(traderName, traderType, getRand(MAX_PRICE), getRand(1, MAX_UNITS));
}


string Trader::getName() {
  return traderName;
}


// Get a bid from the user
Bid* Trader::generateManualBid() {
  char type = traderType;

  // Print the name so it is easier to read when there are lots of bids
  cout << getName() << " please make your bid.\n";

  // If a general trader, let the user decide which type of bid it is
  if (traderType == ALL_TRADER) {
    do {
      cout << "Type of bid (B/S): ";
      cin >> type;
    } while ( !(type == BUY || type == SELL) );
  }

  // Select units
  int units;
  do {
    cout << "How many units: ";
    cin >> units;
  } while ( !(units <= MAX_UNITS && units >= MIN_UNITS) );

  // Select price
  double price;
  do {
    cout << "At what price: ";
    cin >> price;
  } while ( !(price <= MAX_PRICE && price >= MIN_PRICE) );

  // Create and return the new bid
  return new Bid(traderName, type, price, units);
}
