#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

class A {
public:
  A () {
    cout << "A constructor called" << endl;
  }
  A (int i) {
    cout << "A constructor called" << endl;
  }

  void succ() {
    cout << "yeet" << endl;
  }

  ~A () {
    cout << "A destructor called" << endl;
  }
};


class B : public A {
public:
  B (int ii) {
    cout << "B constructor called" << endl;
  }

  void succ() {
    A::succ();
    cout << "YEET" << endl;
  }

  ~B () {
    cout << "B destructor called" << endl;
  }
};


class C {
public:
  int n;
  B b;
  A a;

  C (int i) : b(i), a(i) {
    cout << "C constructor called" << endl;
    n = i;
  }

  int getN() const {
    return n;
  }

  /*
  // Compiler error
  int getNpp() const {
    n++;
    return n;
  }
  */

  ~C () {
    cout << "C destructor called" << endl;
  }
};


template <class DT>
class Gay {
public:
  DT succ;

  Gay(DT mysucc) {
    succ = mysucc;
    cout << "Hello you gay haha xd" << endl;
  }

  void printsucc() {
    cout << succ << endl;
  }
};


template <class type>
int gayLol(type whatev) {
  cout << "Gay function lol" << endl;
  cout << whatev << endl;
  return 69;
}


class Animal {
public:
  int age;
  int numLegs;
  string name;

  Animal(int a, int nL, string n) : age(a), numLegs(nL), name(n) {}
  virtual void matingCall() = 0;
};


class Cat : public Animal {
public:
  Cat(int a, int nL, string n) : Animal(a, nL, n) {}
  virtual void matingCall() {
    cout << "Meow..." << endl;
  }
};


void swap(int &n1, int &n2) {
  // n1 and n2 are not pointers, they are abstracted pointers
  cout << "n1: " << n1 << " n2: " << n2 << endl;
  int tmp = n1;
  n1 = n2;
  n2 = tmp;
}


int operator+(Cat a, Cat b) {
  return a.age + b.age;
}


int main() {
  srand(time(NULL));

  cout << "Hello world" << endl;
  int n = 6;
  string s = "hhhhhhh";
  cout << &n << s << endl;

  s[1] = 'g';
  cout << s << endl;

  char* c = &s[1];
  *c = '6';
  cout << s << endl;
  c++;
  *c = '9';
  cout << s << endl;

  int g[n];
  int* k;
  for (int i = 0; i < n; i++) {
    k = g + i;    // int* overloads + by auto incrementing sizeof
    *k = rand();
    cout << g[i] << " ";
  }
  cout << endl;

  C weird(69);
  cout << weird.getN() << endl;
  weird.b.succ();
  //weird = *new C(68); // Never do this, cannot delete and never garbage collected

  swap(n, g[2]);
  cout << n << endl << g[2] << endl;

  Gay<int> gay(69);
  gay.printsucc();

  Gay<double> gay2(69.69);
  gay2.printsucc();

  gayLol<string>("1");


  Cat cat(6, 69, "Jenkins"), cat2(1, 2, "three");
  cat.matingCall();


  n = 3;
  int **arr;
  arr = new int*[n];
  for (int i = 0; i < n; i++) {
    arr[i] = new int[n];

    for (int j = 0; j < n; j++) {
      arr[i][j] = rand();
      cout << arr[i][j] << " ";
    }
    cout << endl;
  }


  for (int i = 0; i < n; i++) {
    delete arr[i];
  }
  delete[] arr;


  cout << cat + cat2 << endl;


  cout << "Yo we done cout << cout..." << endl;
  cout << cout << endl;
  return 0;
}
