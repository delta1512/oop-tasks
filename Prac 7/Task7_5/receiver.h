#include "Date.h"

#ifndef KATE_H_
#define KATE_H_

class Receiver {
	string message;
	Date** recvDates;
public:
	void receiveMessage(string ss) {message = ss;}
	void receiveDates(Date** dates) {recvDates = dates;}
	void printMessage() {cout << "You said: " << message << endl;}
	void printDates();
};


void Receiver::printDates() {
	cout << "Received dates are: " << endl;
	for (int i = 0; i < 100; i++) {
		recvDates[i]->showdate();
	}
}
#endif /* KATE_H_ */
