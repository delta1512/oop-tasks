#include "Date.h"
#include "random.h"

#ifndef JONE_H_
#define JONE_H_

class Sender {
	string message;
	Date** dates;
public:
	void setMessage();
	void setDates();
	string getMessage() {return message;}
	Date** getDates() {return dates;}

	~Sender();
};

void Sender::setMessage() {
	string ss;
	cout << "Sender says:";
	getline(cin, ss);
	message = ss;
}


void Sender::setDates() {
	// Randomly generate dates
	dates = new Date*[100];
	int d, m, y;

	for (int i = 0; i < 100; i++) {
		m = getRand(1, 12);
		d = getRand(1, days[m]);
		y = getRand(2000, 2019);
		dates[i] = new Date(d, m, y);
	}
}


Sender::~Sender() {
	for (int i = 0; i < 100; i++) {
		delete dates[i];
	}
	delete[] dates;
}


#endif /* JONE_H_ */
