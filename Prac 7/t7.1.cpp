#include <iostream>
using namespace std;

class A {
  int i;
public:
  A(int ii) : i(ii) {cout << "Hey this is A being created!" << endl;}
  ~A() {cout << "Hey bro, you just deleted A!" << endl;}
  void f() const {}
};

class B {
  int i;
public:
  B(int ii) : i(ii) {cout << "Hey this is B being created!" << endl;}
  ~B() {cout << "Hey bro, you just deleted B!" << endl;}
  void f() const {}
};

class C : public B {
  A a;
  B b;
public:
  C(int ii) : b(ii), a(ii), B(ii) {cout << "Hey this is C being created!" << endl;}
  ~C() {cout << "Hey bro, you just deleted C!" << endl;}
  void f() const {
    a.f();
    B::f();
  }
};

int main() {
  C c(47);

  /*
  Hey this is B being created!
  Hey this is A being created!
  Hey this is C being created!
  Hey bro, you just deleted C!
  Hey bro, you just deleted A!
  Hey bro, you just deleted B!

  1. B(ii) initialises B, the base class of C
  2. a(ii) initialises a member of C which is of type A, hence calling its
      constructor.
  3. Constructor of the C object is finally called
  4. C's destructor is called first
  5. a is now out of scope so it must call its destructor
  6. C's base class B then calls its destructor
  */

  return 0;
}
