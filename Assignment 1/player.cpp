/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include "player.h"


// CONSTRUCTOR
// startX,startY: Starting placement of the player
// ico: icon for the player
Player::Player(int startX, int startY, char ico) {
  // Assign inputs to data members
  xPos = xInit = startX;
  yPos = yInit = startY;
  icon = ico;

  // Figure out which player this is
  if (icon == P1_ICON) {
    // Player 1 wil lalways start down
    direction = startDir = DOWN;
  } else {
    // Player 2 will always start up
    direction = startDir = UP;
  }
}


int Player::getX() {
  return xPos;
}


int Player::getY() {
  return yPos;
}


char Player::getIcon() {
  return icon;
}


int Player::getScore() {
  return score;
}


// Adds the 2D vector of direction to the x and y components of the player
void Player::move() {
  xPos += direction[0];
  yPos += direction[1];
}


// Turning the same direction you are going or turning backwards is invalid
bool Player::isValidDirection(const int* dir) {
  return !((dir[0] == direction[0]) || (dir[1] == direction[1]));
}


// Turns a character u, d, l, r to a 2D direction vector
const int* Player::getDirectionFromChar(char dir) {
  switch (dir) {
    case 'u':
      return UP;
    case 'd':
      return DOWN;
    case 'l':
      return LEFT;
    case 'r':
      return RIGHT;

    // If invalid input, assume going in the same direction
    default:
      return direction;
  }
}


// Checks all possible moves that the player can perform
// numMoves: the number of possible moves (used length of array)
// possibleMoves: the array of u,d,l,r which is added to
//              eg, if l,r is valid, it will be [l, r, _, _]
void Player::getPossibleMoves(int &numMoves, char possibleMoves[], Board &board) {
  // Go through all the directions
  int x, y;
  const int* currentDirection;
  for (int i = 0; i < 4; i++) {

    // Calculate the resulting move
    currentDirection = getDirectionFromChar(MOVES[i]);
    x = currentDirection[0] + xPos;
    y = currentDirection[1] + yPos;

    // If this new move is valid, add it to the possible moves
    if ( board.isInBoard(x, y) && (board.getBoard()[x][y] == SPACE) ) {
      possibleMoves[numMoves++] = MOVES[i];
    }

  }
}


// Changes the direction of the current player based on input
void Player::changeDirection(char newDirection) {
  // Convert the direction to an array
  const int* newDirectionArray = getDirectionFromChar(newDirection);

  // Check if it is valid, then set it
  if (isValidDirection(newDirectionArray)) {
    direction = newDirectionArray;
  }
}


// If a player wins, add 1 to their score
void Player::win() {
  score++;
}


// Put the player back into their starting position.
// Used when starting a new game
void Player::reset() {
  xPos = xInit;
  yPos = yInit;
  direction = startDir;
}
