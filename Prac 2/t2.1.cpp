#include <iostream>

using namespace std;


void printOdd(int start, int end);
int getSumOfEven(int start, int end);


int main() {
  int firstNum = 0;
  int secondNum = 0;

  while (firstNum >= secondNum) {
    cout << "Type an integer: ";
    cin >> firstNum;
    cout << "Type another integer: ";
    cin >> secondNum;
  }

  cout << "All odd numbers between " << firstNum << " and " << secondNum << endl;

  printOdd(firstNum, secondNum);

  int sum = getSumOfEven(firstNum, secondNum);

  cout << "\nSum of even numbers between firstNum and secondNum: " << sum << endl;

  return 0;
}


void printOdd(int start, int end) {
  int count = start + 1;
  while (count < end) {
    if ((count % 2) != 0) {
      cout << count << " ";
    }
    count++;
  }
}


int getSumOfEven(int start, int end) {
  int sum = 0;
  int count = start + 1;
  while (count < end) {
    if ((count % 2) == 0) {
      sum += count;
    }
    count++;
  }

  return sum;
}
