#include <iostream>

#include "buyer.h"
#include "seller.h"
#include "bid.h"
#include "auctioneer.h"
#include "trade.h"
#include "random.h"

#ifndef SIM_H
#define SIM_H


class Simulator {
  Buyer** buyers; // The array of buyers
  int nBuyers; // The length of the above array
  Seller** sellers; // The array of sellers
  int nSellers; // The length of the above array
  Bid** buys; // The array of buy orders
  int nBuys; // The length of the above array
  Bid** sells; // The array of sell orders
  int nSells; // The length of the above array
  // Initialise Auctioneer as null pointer
  Auctioneer* auctioneer = NULL;
  bool ordersCleared;

  void clearOrders();

  public:
    Simulator(int numBuyers, int numSellers);
    void run(int maxOrders);
    void doAuction();
    void getBuyOrders(int n, bool manual=false);
    void getSellOrders(int n, bool manual=false);
    ~Simulator();
};


#endif
