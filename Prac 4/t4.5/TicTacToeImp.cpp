/*
TODO: make const size
replace 3s with size
replace 2s with size-1
*/

//Tic-Tac-Toe Implementation file

#include <iostream>
#include <iomanip>

#include "TicTacToe.h"

using namespace std;

void ticTacToe::play()
{
    bool done = false;
    char player = 'X';

    displayBoard();

    while (!done)
    {
        done = getXOMove(player);

        if (player == 'X')
            player = 'O';
        else
            player = 'X';
    }
}

void ticTacToe::displayBoard() const {
  int chars = 3; // variable width for numbers with 2-digits
  // Print the numbers at the top
  for (int col = 0; col < BOARDSIZE; col++) {
    cout << setw(4) << col+1;
  }
  cout << endl;

  // Go through each row
  for (int row = 0; row < BOARDSIZE; row++) {

    // Space to align board
    cout << setw(2) << "";
    // Place the separators (except for last)
    for (int col = 0; col < BOARDSIZE-1; col++) {
      cout << setw(4) << "|";
    }

    // Calculate the amount of chars in the row number
    // This handles the case where there are 2-digit numbers
    if (row+1 >= 10) {
      chars = 2;
    }

    // Print the row number and first value
    cout << endl << row+1 << setw(chars) << board[row][0];
    // Print separators and values (skip the first)
    for (int col = 1; col < BOARDSIZE; col++) {
      cout << " |" << setw(2) << board[row][col];
    }
    cout << endl;

    // If last row, don't print underscores
    if (row == BOARDSIZE-1) {
      cout << setw(2) << "";
      for (int col = 0; col < BOARDSIZE-1; col++) {
        cout << setw(4) << "|";
      }
    } else { // Else print underscores
      cout << setw(5) << "___";
      for (int col = 0; col < BOARDSIZE-1; col++) {
        cout << "|___";
      }
    }
    cout << endl;
  }
}

bool ticTacToe::isValidMove(int row, int col) const
{
    if (true /*See your solution to Practical 3 Task 3.3 or ask your tutor for a solution */)
        return true;
    else
        return false;
}

bool ticTacToe::getXOMove(char playerSymbol)
{
    int row, col;

    do
    {
        cout << "Player " << playerSymbol
             << " enter move: ";
        cin >> row >> col;
        cout << endl;
    }
    while (!isValidMove(row - 1, col - 1) );

    row--;
    col--;

    noOfMoves++;

    board[row][col] = playerSymbol;
    displayBoard();

    status gStatus = gameStatus();

    if (gStatus == WIN)
    {
        cout << "Player " << playerSymbol << " wins!" << endl;
        return true;
    }
    else if (gStatus == DRAW)
    {
        cout << "This game is a draw!" << endl;
        return true;
    }
    else if(noOfMoves >= MAX_MOVES) {
    	return true;
    } else
      return false;
}


status ticTacToe::gameStatus() {
  status result = CONTINUE;
  if (noOfMoves == MAX_MOVES) {
    result = DRAW;
  }

  //consider horizontal and vertical wins
  char current, prev;
  bool same = true; // keeps track of whether a line of values is the same
  int i, j = 0;
  for (i = 0; i < BOARDSIZE; i++) {
    // Check the current row
    while (same && (j < BOARDSIZE-1)) {
      prev = board[i][j++];
      current = board[i][j];
      same = same && (prev == current) && (prev == 'X' || prev == 'O');
    }

    if (same) {
      return WIN;
    }


    j = 0;
    same = true;
    // Check the current column
    while (same && (j < BOARDSIZE-1)) {
      prev = board[j++][i];
      current = board[j][i];
      same = same && (prev == current) && (prev == 'X' || prev == 'O');
    }

    if (same) {
      return WIN;
    }
  }

  // Check diagonal 1
  i = 0;
  j = 0;
  same = true;
  while (same && (j < BOARDSIZE-1)) {
    prev = board[i++][j++];
    current = board[i][j];
    same = same && (prev == current) && (prev == 'X' || prev == 'O');
  }

  if (same) {
    return WIN;
  }

  // Check diagonal 2
  i = 0;
  j = 0;
  same = true;
  while (same && (j < BOARDSIZE-1)) {
    prev = board[i++][BOARDSIZE-1-j++];
    current = board[i][BOARDSIZE-1-j];
    same = same && (prev == current) && (prev == 'X' || prev == 'O');
  }

  if (same) {
    return WIN;
  }

  return result;
}

void ticTacToe::reStart()
{
    for (int row = 0; row < BOARDSIZE; row++)
        for (int col = 0; col < BOARDSIZE; col++)
            board[row][col] = ' ';

    noOfMoves = 0;
}

ticTacToe::ticTacToe()
{
    for (int row = 0; row < BOARDSIZE; row++)
        for (int col = 0; col < BOARDSIZE; col++)
            board[row][col] = ' ';

    noOfMoves = 0;
}
