#include <iostream>

using namespace std;

int main() {
  int firstNum = 0;
  int secondNum = 0;

  while (firstNum >= secondNum) {
    cout << "Type an integer: ";
    cin >> firstNum;
    cout << "Type another integer: ";
    cin >> secondNum;
  }

  cout << "All odd numbers between " << firstNum << " and " << secondNum << endl;

  int count = firstNum + 1;
  while (count < secondNum) {
    if ((count % 2) != 0) {
      cout << count << " ";
    }
    count++;
  }

  int sum = 0;
  count = firstNum + 1;
  while (count < secondNum) {
    if ((count % 2) == 0) {
      sum += count;
    }
    count++;
  }

  cout << "\nSum of even numbers between firstNum and secondNum: " << sum << endl;

  return 0;
}
