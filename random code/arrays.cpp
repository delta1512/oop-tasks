#include <iostream>

using namespace std;


int main() {
  int A[3] = {0, 1, 2};

  cout << sizeof(A) << endl; // Size of the array (bytes)

  cout << sizeof(A[1]) << endl; // Size of one integer (bytes)

  cout << A << endl; // The address of the array (hex)

  cout << A[2] << endl;

  int *p = &A[1]; // Get a pointer to the int at index 1
  cout << p << endl;

  return 0;
}
