/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <iostream>

#include "humanPlayer.h"

using namespace std;

// CONSTRUCTOR
// Calls the Player constructor with the respective arguments
HumanPlayer::HumanPlayer(int startX, int startY, char ico) : Player(startX, startY, ico) {}


// Gets a new direction to move by prompting the user and collecting input
char HumanPlayer::getNewDirection(Board &board) {
  char choice;
  do {

    cout << "\n\tPlayer " << icon << " move (u, d, l or r): ";
    cin >> choice;

  // While choice is valid
  } while (! ((choice == 'u') || (choice == 'd') ||
              (choice == 'l') || (choice == 'r'))   );

  return choice;
}
