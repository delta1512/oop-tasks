#include <iostream>

using namespace std;


class ProductionGraph {
  int* production;
  int plants;

  public:
    ProductionGraph(int numPlants) {
      // Save the length of the array
      plants = numPlants;
      // Dynamically allocate memory for a new int array
      production = new int[numPlants];
    }


    void inputData() {
      int amount;
      for (int i = 1; i <= plants; i++) {
        cout << endl << "Enter production data for plant number " << i << endl;

        cout << "Enter a positive integer of units produced by each department, ranging from 1-20.\n\n";
        cin >> amount;

        // Input-checking while-loop
        while (amount < 0) {
      	  cout<<"input again" << endl;
          cin >> amount;
        }

        // Save the amount to the array
        production[i-1] = amount;
      }
    }


    void graph() {
        cout << "\nUnits produced in units:\n";

        for (int i = 1; i <= plants; i++) {
          cout << "Plant #" << i << " ";

          for (int count = 0; count < production[i-1]; count++) {
            // Display units
            cout << "*";
          }

          cout << endl;
        }
    }


    // Deallocate the memory for the array
    ~ProductionGraph() {
      delete[] production;
    }
};


int main() {
  int numPlants;
  cout << "Enter the number of plants: ";
  cin >> numPlants;
  cout << endl;

  // Initialise an object with the number of plants
  ProductionGraph business(numPlants);

  // Collect input data
  business.inputData();
  // Graph the data
  business.graph();

  return 0;
}
