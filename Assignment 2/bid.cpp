/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

#include "bid.h"

using namespace std;


// Initialise the static int
int Bid::numBids = 0;


// CONSTRUCTOR
// Initialises all of the relevant variables
Bid::Bid(string name, char type, double p, int q) {
  traderName = name;
  // When making a new bid, increment the ID
  bidId = ++numBids;
  bidType = type;
  price = p;
  quantity = q;
}


// Outputs a string representation of the bid so that it can be printed or stored
string Bid::toString() {
  ostringstream stream;
  stream << "(\"" << traderName << "\", "
    << to_string(bidId) << ", '"
    << bidType << "', "
    << setw(MAX_PRICE_W) << to_string(price) << ", "
    << setw(MAX_UNITS_W) << to_string(quantity) << ")";
  string finalStr = stream.str();
  return finalStr;
}


// Easy printing function for the bid
void Bid::print() {
  cout << toString() << endl;
}


// Fills the bid according to the other bid that filled it
//  units: the number of units to fill (quantity of other bid)
//  otherPrice: price of the other bid
//  clearPrice: (returned) the clearing price of the trade
//  unitsLeft: (returned) the new quantity of the other bid after filling this one
void Bid::fill(int units, double otherPrice, double &clearPrice, int &unitsLeft) {
  quantity -= units;
  if (isFilled()) {
    unitsLeft = abs(quantity);
    quantity = 0;
  } else {
    unitsLeft = 0;
  }

  clearPrice = (price + otherPrice) / 2;
}


// For use after the above is complete to ensure that the other bid is filled
// with the amount of units it filled in the bid.
void Bid::fill(int units) {
  quantity -= units;
}


bool Bid::isFilled() {
  return quantity <= 0;
}


// Cancels a bid
void Bid::cancel() {
  quantity = 0;
}


int Bid::getId() {
  return bidId;
}


string Bid::getName() {
  return traderName;
}


char Bid::getType() {
  return bidType;
}


double Bid::getPrice() {
  return price;
}


int Bid::getQuantity() {
  return quantity;
}
