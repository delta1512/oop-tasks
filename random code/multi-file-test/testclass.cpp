#include <iostream>

using namespace std;



// Structs have all attributes public
struct World {
  string name;
  string planet;
  long population;
  double lifeRating;
};


// Classes always default to private but you can specify
class Animal {
  // These variables are only accessible from the functions below
  double health = 10;
  double hunger = 10;
  double sleep = 10;
  World home;

  public: // We want these functions to be public
    void eat(double hungerValue) {
      hunger += hungerValue;
    }

    void goToSleep() {
      sleep += 10;
      hunger -= 3.5;
      health += 1.5;
    }

    bool isDead() {
      return (hunger < 0) || (health < 0) || (sleep < 0);
    }

    void setWorld(World newWorld) {
      home = newWorld;
    }

    void getWorld() {
      cout << home.name << endl;
    }
};


// Adding public before Animal means keep all access rules the same
//    protected would mean public attributes from Animal will become protected
//    private would mean public and protected attributes will become private
class Doge: public Animal {
  string name;

  public:
    void bark() {
      cout << "Woof! My name is " << name << endl;
    }

    void setName(string newName) {
      name = newName;
    }
};
