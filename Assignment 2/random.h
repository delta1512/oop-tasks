#include <cstdlib>
#include <string>
#include <ctime>

#ifndef RANDOM_H
#define RANDOM_H

// Integer random getters
int getRand(int a, int b);
int getRand(int n);
// Double random getters
double getRand(double a, double b);
double getRand(double n);
// Random name getter
std::string getRandName();


#endif
