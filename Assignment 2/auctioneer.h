#include <fstream>
#include <string>

#include "bid.h"
#include "trade.h"
#include "constants.h"

#ifndef AUCT_H
#define AUCT_H


class Auctioneer {
  static int nAucts; // Number of auctions that have occurred
  Bid** buys; // Array of buy orders
  int nBuys;  // The length of the above array
  Bid** sells; // Array of sell orders
  int nSells; // The length of the above array

  Bid* getTopBuyBid();
  Bid* getTopSellBid();
  void storeBids(std::ofstream &of, std::string startMsg);
  void storeTrades(std::ofstream &of, Trade** trades, int nTrades);
  Trade* match();

  public:
    // Constructor simply assigns all of the values required
    Auctioneer(Bid** b, int nB, Bid** s, int nS) : buys(b), nBuys(nB), sells(s), nSells(nS) {};
    void run();
    // reset() simply assigns all of the values required.
    // Tis is more efficient than destruction and reconstruction
    void reset(Bid** b, int nB, Bid** s, int nS);
    int getNumAucts();
    // No destructor needed because buys and sells are handled by the simulator
    // or class/program this class is contained in.
};


#endif
