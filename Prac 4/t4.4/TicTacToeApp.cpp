
//Main program

#include <iostream>

#include "TicTacToeImp.cpp"

using namespace std;

int main()
{
     ticTacToe game;

     game.play();

     return 0;
}
