#include <fstream>
#include <sstream>

#include <iostream>

using namespace std;

int main() {
  ifstream mathSheet("mathsheet.txt");
  ofstream answerSheet("answersheet.txt");
  string line;
  istringstream lineStream;

  int a, b;
  char tmp;
  while (getline(mathSheet, line)) {
    lineStream.str(line);
    lineStream >> a >> tmp >> tmp >> b;
    lineStream.clear();

    answerSheet << line << " " << a*b << endl;
  }

  mathSheet.close();
  answerSheet.close();
}
