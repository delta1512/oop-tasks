#include <iostream>

using namespace std;


void testOverloading(int numerator, int denominator);
void testOverloading(double numerator, double denominator);
void testOverloading(int numerator, double denominator);


int main() {
  int int1 = 3;
  int int2 = 2;
  double db1 = 3.0;
  double db2 = 2.0;

  // Truncates the decimal (0.5) because this is integer division so
  // the result must be an integer
  testOverloading(int1, int2);

  // Keeps all decimals because this is double division so the result is
  // a double, a data structure capable of storing the decimals
  testOverloading(db1, db2);

  // Because a double is used in the division, C++ produces a double output
  // which is the exact same as the above function.
  testOverloading(int1, db2);

  return 0;
}


void testOverloading(int numerator, int denominator) {
  int fraction = numerator / denominator;
  cout << "Fraction1 = " << fraction << endl;
}


void testOverloading(double numerator, double denominator) {
  double fraction = numerator / denominator;
  cout << "Fraction2 = " << fraction << endl;
}


void testOverloading(int numerator, double denominator) {
  double fraction = numerator / denominator;
  cout << "Fraction3 = " << fraction << endl;
}
