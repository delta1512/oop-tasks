#include <string>

#include "trader.h"
#include "constants.h"

#ifndef SELLER_H
#define SELLER_H


class Seller : public Trader {
  public:
    Seller(std::string n);
};


#endif
