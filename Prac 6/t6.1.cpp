#include <iostream>

using namespace std;


class foo {
  // Static int to track number of foos
  static int numFoos;

  public:
    // Constructor increments static member and outputs it
    foo() {
      cout << "This is foo number " << ++numFoos << endl;
    }

    // Prints how many foos there are
    void printFoo() {
      cout << "There currently are " << numFoos << " foos" << endl;
    }
};

// Define the static member outside the class descriptor
int foo::numFoos = 0;


int main() {
  // Create two foos
  foo bar1;
  foo bar2;

  // Print how many foos there are (both should print 2)
  bar1.printFoo();
  bar2.printFoo();

  return 0;
}
