#include <iostream>
#include <math.h>

using namespace std;

double pi = 3.14; // Global constant pi (we need to give reasons for global variables)

long long fact(long long);
void twodigit(int&, int&, int&);
void type(int);
void type(double);

int main() {
  long long f;
  f = fact(20);
  cout << f << endl;

  cout << pow(2, 3) << endl;

  cout << pow(0.5, 2) << endl;

  int test1 = 3;
  double test2 = 0.5;
  cout << test1*test2 << endl;

  int aa;
  int bb;
  int nn = 14;
  twodigit(nn, aa, bb);
  cout << aa << bb << endl;

  type(aa);
  type(test2);
}

long long fact(long long n) {
    if (n == 1){
        return 1;
    } else {
        return n*fact(n-1);
    }
}

void twodigit(int& nn, int& aa, int& bb) {
    aa = nn / 10;
    bb = nn % 10;
}

void type(int nn) {
    cout << "this is an integer" << endl;
}

void type(double nn) {
    cout << "this is aa double" << endl;
}
