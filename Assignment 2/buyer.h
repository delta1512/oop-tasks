#include <string>

#include "trader.h"
#include "constants.h"

#ifndef BUYER_H
#define BUYER_H


class Buyer : public Trader {
  public:
    Buyer(std::string n);
};


#endif
