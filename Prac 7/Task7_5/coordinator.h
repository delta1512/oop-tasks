#include "Date.h"

#ifndef COORDINATOR_H_
#define COORDINATOR_H_

class Coordinator {
	Sender sender;
	Receiver receiver;
public:
	void run();
};

void Coordinator::run() {
	/*
	// Original code
	sender.setMessage(); // the sender set a message
  receiver.receiveMessage(sender.getMessage()); // get message from the sender and pass it to the receiver
  receiver.printMessage();
	*/

	sender.setDates();
	receiver.receiveDates(sender.getDates());
	receiver.printDates();
}
#endif /* COORDINATOR_H_ */
