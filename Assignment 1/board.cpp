/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <iostream>
#include <string>

#include "board.h"


// CONSTRUCTOR
Board::Board(int boardSize) {
  size = boardSize;

  // initiate the board with the specified size
  board = new char*[size];

  // Fill with spaces
  for (int i = 0; i < size; i++) {
    board[i] = new char[size];

    for (int j = 0; j < size; j++) {
      board[i][j] = ' ';
    }

  }

}


// Returns the pointer to the board array
char** Board::getBoard() {
  return board;
}


// Determines whether the x,y values provided lie within the bounds of the board
bool Board::isInBoard(int x, int y) {
  return (x >= 0) && (x < size) && (y >= 0) && (y < size);
}


// Finds the index of the previous space the player was in.
// Values are returned as prevX, prevY by reference.
// Uses the new move x and y as a reference and looks around by 1 space.
// x,y: The position the player is going to move to
void Board::getPrevXY(int x, int y, char playerIcon, int &prevX, int &prevY) {
  // Check up
  if (isInBoard(x, y-1) && (board[x][y-1] == playerIcon) ) {
    prevX = x;
    prevY = y-1;

  // Check down
  } else if (isInBoard(x, y+1) && (board[x][y+1] == playerIcon) ) {
    prevX = x;
    prevY = y+1;

  // Check left
  } else if (isInBoard(x-1, y) && (board[x-1][y] == playerIcon) ) {
    prevX = x-1;
    prevY = y;

  // Check right
  } else if (isInBoard(x+1, y) && (board[x+1][y] == playerIcon) ) {
    prevX = x+1;
    prevY = y;

  // Else the game is starting
  } else {
    prevX = -99; // Assign the sentinel value
  }
}


// Prints the contents of the board to the screen
void Board::show() {
  // Create a separator that will be reused
  string sep = string(size*4+1, '-') + "\n";

  // Make some space
  cout << endl << endl;

  // for every row...
  for (int i = 0; i < size; i++) {
    // create a separator and start the first vertical separator
    cout << sep;
    cout << "| ";
    // for every column
    for (int j = 0; j < size; j++) {
      // print the value plus a vertical separator
      cout << board[i][j] << " | ";
    }
    // Next row
    cout << endl;
  }

  // add a final separator
  cout << sep << endl;
}


// Make the board all spaces
void Board::clear() {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      board[i][j] = ' ';
    }
  }
}


// Moves the player icon on the board
// x,y: The position the player is going to move to
void Board::updateBoard(int x, int y, char playerIcon) {
  // 1. Find the current player
  int prevX, prevY;
  getPrevXY(x, y, playerIcon, prevX, prevY);

  // If the game only just started, place the icons
  // -99 is sentinel value
  if (prevX == -99) {
    board[x][y] = playerIcon;

  // Else the game has already initialised
  } else {
    // 2. Replace icon with trail
    board[prevX][prevY] = TRAIL;

    // 3. Add new icon if in the board and it doesn't overwrite a trail
    //    and doesn't overwite a collision
    if ( isInBoard(x, y) && (board[x][y] != TRAIL) && (board[x][y] != EXPLODE) ) {
      board[x][y] = playerIcon;
    }
  }
}


// Handles moving the player around on the board and checking result of moves.
// x,y: The position the player is going to move to
status Board::processMove(int x, int y, char playerIcon) {
  status otherPlayer, result = CONTINUE;
  char otherPlayerIcon;
  // Determine which is the other player in case the current player loses
  if (playerIcon == P1_ICON) {
    otherPlayer = P2;
    otherPlayerIcon = P2_ICON;
  } else {
    otherPlayer = P1;
    otherPlayerIcon = P1_ICON;
  }

  // Handle game winning or losing events
  // Case where player hits boundary
  if (!isInBoard(x, y)) {
    result = otherPlayer;

  // Case where player hits trail
  } else if (board[x][y] == TRAIL) {
    result = otherPlayer;

  // Case where players collide
  } else if (board[x][y] == otherPlayerIcon) {
    // Draw an explosion on the board on collision
    board[x][y] = EXPLODE;
    result = DRAW;
  }

  // Update the board
  updateBoard(x, y, playerIcon);

  return result;
}


// DESTRUCTOR
Board::~Board() {
  // Delete all pointers to char arrays
  for (int i = 0; i < size; i++) {
    delete[] board[i];
  }

  // Delete the pointer to the array of pointers
  delete board;
}
