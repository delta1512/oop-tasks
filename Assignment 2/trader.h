#include <iostream>

#include "bid.h"
#include "constants.h"
#include "random.h"

#ifndef TRADER_H
#define TRADER_H


class Trader {
  protected:
    std::string traderName;
    char traderType;

  public:
    Trader(std::string n, char t) : traderName(n), traderType(t) {};
    Bid* generateBid();
    std::string getName();
    Bid* generateManualBid();
};


#endif
