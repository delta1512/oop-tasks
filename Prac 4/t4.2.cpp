#include <iostream>

using namespace std;


class Student {
  private:
    int studentId;
    string studentName;
    int courseNo;
    bool enrolmentstatus;

  public:
    Student(int id, string name, int course, bool enrol);
    void display();
};


Student::Student(int id, string name, int course, bool enrol) {
  studentId = id;
  studentName = name;
  courseNo = course;
  enrolmentstatus = enrol;
}

void Student::display() {
  cout << "Student " << studentName << ": " << endl;
  cout << "\tID: " << studentId << endl;
  cout << "\tcourse: " << courseNo << endl;

  cout << "\tCurrently ";
  // If not enrolled, just say not
  if (!enrolmentstatus) {
    cout << "not ";
  }
  cout << "enrolled" << endl;
}


// This is the driver function
int main() {
  Student Calum(19694, "Calum Murry", 3000619, true);
  Calum.display();

  Student Roxy(19695, "Roxy Smith", 3008969, false);
  Roxy.display();
}
