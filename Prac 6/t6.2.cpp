#include <iostream>
#include <ctype.h>

using namespace std;


string trim(string input);


int main() {
  string testString;

  getline(cin, testString);

  cout << trim(testString) << endl;

  return 0;
}


string trim(string input) {
  string finalString;
  char currentChar;

  // For every character
  for (int i = 0; i < input.length(); i++) {
    // Get the current character
    currentChar = input[i];
    // If it is alphabetic...
    if (isalpha(currentChar)) {
      // Add it to the output string
      finalString += currentChar;
    }
  }

  return finalString;
}
