#include "player.h"

#ifndef RandomPlayer_H
#define RandomPlayer_H


class RandomPlayer: public Player {
  protected:
    int getRand(int a, int b);
  public:
    RandomPlayer(int startX, int startY, char ico);
    char getNewDirection(Board &board);
};


#endif
