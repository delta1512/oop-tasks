/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

#include "trade.h"


using namespace std;


string Trade::toString() {
  // | price | units | total amount | maker Order ID | taker Order ID |

  // Place the string into a stream to make it easier to manipulate
  ostringstream stream;
  stream << "| $" << setw(MAX_PRICE_W) << clearPrice << " | "
          << setw(MAX_UNITS_W) << volume << " | $"
          << setw(MAX_VOL_W) << volume*clearPrice << " | "
          << makerOID << " | "
          << takerOID << " |";
  // Convert back to string
  string finalStr = stream.str();
  return finalStr;
}
