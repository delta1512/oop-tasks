/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include <cstdlib>

#include "randomPlayer.h"

using namespace std;


// CONSTRUCTOR
// Calls the Player constructor with all required parameters
RandomPlayer::RandomPlayer(int startX, int startY, char ico) : Player(startX, startY, ico) {}


// Returns a random number between a and b (inclusive)
int RandomPlayer::getRand(int a, int b) {
  return rand() % (b-a+1) + a;
}


// Gets all the possible moves and chooses one
char RandomPlayer::getNewDirection(Board &board) {
  int numMoves = 0;
  char possibleMoves[4];
  getPossibleMoves(numMoves, possibleMoves, board);

  // If there are no possible moves, choose any because you are screwed either way
  if (numMoves == 0) {
    return MOVES[getRand(0, 3)];
  } else {
    return possibleMoves[getRand(0, numMoves-1)];
  }
}
