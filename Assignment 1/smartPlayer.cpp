/*********** Declaration*******
I hereby certify that no part of this assignment has been copied from
any other student’s work or from any other source. No part of the code
has been written/produced for me by another person or copied from any
other source.

I hold a copy of this assignment that I can produce if the original is
lost or damaged.
**************************/

#include "smartPlayer.h"


// CONSTRUCTOR
// Calls the Player constructor with all required parameters
SmartPlayer::SmartPlayer(int startX, int startY, char ico) : Player(startX, startY, ico) {}


// Checks whether a particular space is a dead end
bool SmartPlayer::isDeadEnd(int x, int y, Board &board) {
  // Assume dead-end unless otherwise
  bool result = true;

  const int* currentDirection;
  int tmpx, tmpy;
  // Go through all the possible moves
  for (int i = 0; i < 4; i++) {
    // Get the next direction to move
    currentDirection = getDirectionFromChar(MOVES[i]);
    tmpx = x + currentDirection[0];
    tmpy = y + currentDirection[1];
    // If it is not in board and not a space, then it is dead end
    result = result && !(board.isInBoard(tmpx, tmpy) &&
                        board.getBoard()[tmpx][tmpy] == SPACE);
  }

  return result;
}


// Chooses the longest path in any direction so long as the next step is not
// a dead end. But if the only way is a dead end, it will choose it.
char SmartPlayer::getNewDirection(Board &board) {
  // Load the final values
  int bestPath = 0;
  int bestPathLength = 0;

  // Search for the longest path
  const int* currentDirection;
  int currentLength = 0;
  int tmpx, tmpy, oneStepX, oneStepY;
  for (int i = 0; i < 4; i++) {

    // Get the next direction
    currentDirection = getDirectionFromChar(MOVES[i]);
    // Reset the temporary positions
    tmpx = xPos;
    tmpy = yPos;

    // Check for free space in a particular direction
    do {
      tmpx += currentDirection[0];
      tmpy += currentDirection[1];
      currentLength++;
    // Keep going until you hit a wall, player or trail
    } while (  board.isInBoard(tmpx, tmpy) &&
              (board.getBoard()[tmpx][tmpy] == SPACE) );

    // Fix off-by-one bug
    currentLength--;

    oneStepX = xPos + currentDirection[0];
    oneStepY = yPos + currentDirection[1];
    // Check for new maximum or equal path but don't choose it if it is dead end
    // but if we have no other way to go, choose the dead end.
    if ( (currentLength >= bestPathLength) &&
          (!isDeadEnd(oneStepX, oneStepY, board) || bestPathLength == 0) ) {
      bestPath = i;
      bestPathLength = currentLength;
    }

    // Reset currentLength
    currentLength = 0;

  }

  // Return the move that provides the best outcome
  return MOVES[bestPath];
}
