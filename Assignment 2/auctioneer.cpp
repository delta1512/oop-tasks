/*
Marcus Belcastro - 19185398

I hereby certify that no part of this assignment has been copied from any other student’s work or
from any other source. No part of the code has been written/produced for me by another person
or copied from any other source.

I hold a copy of this assignment that I can produce if the original is lost or damaged.
*/

#include "auctioneer.h"


using namespace std;


// Initialise the static int for number of auctions
int Auctioneer::nAucts = 0;


// Maximum search algorithm to find the best buy order (highest price)
Bid* Auctioneer::getTopBuyBid() {
  Bid* topBid = NULL;
  double topBidPrice = 0.0;

  Bid* currentBid;
  double currentPrice;
  for (int i = 0; i < nBuys; i++) {
    currentBid = buys[i];
    currentPrice = currentBid->getPrice();
    // If new max and non-filled bid
    if ( (currentPrice > topBidPrice) && (!currentBid->isFilled()) ) {
      // Then the current bid is the best bid
      topBid = currentBid;
      topBidPrice = currentPrice;
    }
  }

  return topBid;
}


// Minimum search algorithm to find the best sell order (lowest price)
Bid* Auctioneer::getTopSellBid() {
  Bid* topBid = NULL;
  double topBidPrice = MAX_PRICE;

  Bid* currentBid;
  double currentPrice;
  for (int i = 0; i < nSells; i++) {
    currentBid = sells[i];
    currentPrice = currentBid->getPrice();
    // If new min and non-filled bid
    if ( (currentPrice < topBidPrice) && (!currentBid->isFilled()) ) {
      // Then the current bid is the best bid
      topBid = currentBid;
      topBidPrice = currentPrice;
    }
  }

  return topBid;
}


// The algorithm that matches all the bids
void Auctioneer::run() {
  // Set up the output file
  ofstream tradeLog;
  tradeLog.open(BASEDIR + to_string(++nAucts) + ".txt");

  // Write a snapshot of the bids before the trades
  storeBids(tradeLog, "---- Trading begins, here are the initial orders ----\n");

  // The maximum number of possible matches is the maximum num of sell/buy orders
  int matches = nBuys + nSells;
  // Create a new array to store all the trades
  Trade** clearedTrades = new Trade*[matches];
  // Count how many trades actually happened
  int nTrades = 0;

  // Start with trying to find one trade
  clearedTrades[nTrades] = match();
  // If a trade happened, keep trying to find more
  while (clearedTrades[nTrades] != NULL) {
    nTrades++;
    clearedTrades[nTrades] = match();
  }

  // Write all the trades to the file
  storeTrades(tradeLog, clearedTrades, nTrades);
  // Write all of the bids after the trades happened
  storeBids(tradeLog, "---- Trading end, here are the final orders ----\n");

  // Garbage collection
  for (int i = 0; i < nTrades; i++) {
    delete clearedTrades[i];
  }
  delete[] clearedTrades;

  // Close the file object
  tradeLog.close();
}


// The algorithm for an individual match between two orders
Trade* Auctioneer::match() {
  // Find the best buy and sell bids
  Bid* topBuy = getTopBuyBid();
  Bid* topSell = getTopSellBid();
  int unitsLeft;
  // If the min/max couldn't find anything, return NULL
  if (topBuy == NULL || topSell == NULL) {
    return NULL;

  // If there is a match or overlapping, perform the process of matching
  } else if (topBuy->getPrice() >= topSell->getPrice()) {
    // Prepare the output
    Trade* currentTrade = new Trade;
    currentTrade->volume = min(topBuy->getQuantity(), topSell->getQuantity());
    // Work out maker vs taker
    if (topBuy->getId() > topSell->getId()) {
      currentTrade->maker = topBuy->getName();
      currentTrade->makerOID = topBuy->getId();
      currentTrade->taker = topSell->getName();
      currentTrade->takerOID = topSell->getId();
    } else {
      currentTrade->maker = topSell->getName();
      currentTrade->makerOID = topSell->getId();
      currentTrade->taker = topBuy->getName();
      currentTrade->takerOID = topBuy->getId();
    }
    // Make the actual trade (this is where all the final values are calculated)
    topBuy->fill(topSell->getQuantity(), topSell->getPrice(),
                  currentTrade->clearPrice, unitsLeft);
    // Calculate residuals or fills for the other side of the trade
    // (This is only to ensure that the bid on the other side is filled too)
    topSell->fill(topSell->getQuantity()-unitsLeft);

    return currentTrade;

  // If it couldn't match, return NULL
  } else {
    return NULL;
  }
}


// Write the bids to a file
// of: the output file stream for the trades and orders
// startMsg (optional): the string to display before the bids are written
void Auctioneer::storeBids(ofstream &of, string startMsg="") {
  of << startMsg;
  for (int i = 0; i < nBuys; i++) {
    of << buys[i]->toString() << endl;
  }
  for (int i = 0; i < nSells; i++) {
    of << sells[i]->toString() << endl;
  }
  of << endl;
}


// Writes all the trades that ocurred
// of: the output file stream for the trades and orders
// trades: the array of pointers of the trades that ocurred
// nTradres: the number of initialised trades in the trades array
void Auctioneer::storeTrades(ofstream &of, Trade** trades, int nTrades) {
  of << "---- Processed orders ----\n";
  of << "| clear price | units | total amount | maker Order ID | taker Order ID |\n";
  for (int i = 0; i < nTrades; i++) {
    of << trades[i]->toString() << endl;
  }
  of << endl;
}


int Auctioneer::getNumAucts() {
  return nAucts;
}


// Set up a new auction
void Auctioneer::reset(Bid** b, int nB, Bid** s, int nS) {
  buys = b;
  nBuys = nB;
  sells = s;
  nSells = nS;
}
