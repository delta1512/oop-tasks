#include <fstream>
#include <string>
#include <vector>

#include <iostream>

#include "Date.h"
#include "random.h"

using namespace std;

int SUMMER[] = {12, 1, 2};
int AUTUMN[] = {3, 4, 5};
int WINTER[] = {6, 7, 8};
int SPRING[] = {9, 10, 11};

bool in(int n, int arr[]);


int main() {
  vector<Date> s1, s2, s3, s4;
  int d, m, y;
  Date currentDate;

  while (s1.size() < 25 || s2.size() < 25 || s3.size() < 25 || s4.size() < 25) {
    y = getRand(2000, 2002);
    // The following implements the 1000 day limit
    // If the year is 2002
    if (y == 2002) {
      // Then you can only get a month between jan and sep
      m = getRand(1, 9);
      // If september is chosen
      if (m == 9) {
        // Then you can only choose between days 1 and 27
        d = getRand(1, 27);

      // Otherwise calculate day like normal
      } else {
        d = getRand(1, days[m]);
      }

    // Otherwise calculate as usual
    } else {
      m = getRand(1, 12);
      d = getRand(1, days[m]);
    }

    currentDate.setDate(d, m, y);
    if (in(m, SUMMER) && s1.size() < 25) {
      s1.push_back(currentDate);
    } else if (in(m, AUTUMN) && s2.size() < 25) {
      s2.push_back(currentDate);
    } else if (in(m, WINTER) && s3.size() < 25) {
      s3.push_back(currentDate);
    } else if (in(m, SPRING) && s4.size() < 25) {
      s4.push_back(currentDate);
    }
  }

  vector<Date> seasons[] = {s1, s2, s3, s4};
  ofstream outFile("out.txt");
  for (int h = 0; h < 4; h++) {
    outFile << "Dates in season " << h+1 << endl;
    for (int i = 0; i < 25; i++) {
      outFile << seasons[h][i].toString() << endl;
    }
    outFile << endl;
  }
  outFile.close();


  return 0;
}


bool in(int n, int arr[]) {
  bool final = false;
  for (int i = 0; i < 3; i++) {
    final = final || arr[i] == n;
  }
  return final;
}
