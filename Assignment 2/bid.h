#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <cmath>

#include "constants.h"

#ifndef BID_H
#define BID_H


class Bid {
  static int numBids; // The number of bids that have ocurred (ensures unique ID)

  std::string traderName;
  int bidId;
  char bidType;
  double price;
  int quantity;

  public:
    Bid(std::string name, char type, double p, int q);
    // Getters
    int getId();
    std::string getName();
    char getType();
    double getPrice();
    int getQuantity();

    void print();
    std::string toString();
    // Fill for this current bid
    void fill(int units, double otherPrice, double &clearPrice, int &unitsLeft);
    // Fill for this bid when it fills another
    void fill(int units);
    bool isFilled();
    void cancel();
};


#endif
